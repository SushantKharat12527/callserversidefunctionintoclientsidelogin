﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ServerFntnToClientSideLOGIN
{
    public partial class LoginPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #region Api
        [WebMethod]
        public static string CheckLoginApi(string userName, string password)
        {
           

            LoginEntity loginEntityObj = new LoginEntity()
            {
                UserName = userName,
                Password = password
            };

            DalRepository dalRepositoryObj = new DalRepository();

           var status= dalRepositoryObj.CheckLoginAsync(loginEntityObj);

            if (status==1)
            {
                return "* User Name & Password Match";
            }
            else
            {
                return "* User Name & Password does not match";
            }

        }
        #endregion
    }
}