﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="ServerFntnToClientSideLOGIN.LoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <style type="text/css">
        #divMessagePanel{
            color:red;
        }

        table{
            margin-left: auto;
    margin-right: auto;
   

        }

        #txtUserName{
                   background: #fff;
    color: #333;
    font-family: inherit;
    height: 35px;
    width: 300px;
    font-size:larger;
        }
        #txtPassword{
                   background: #fff;
    color: #333;
    font-family: inherit;
    height: 35px;
    width: 300px;
    font-size:larger;
        }
        .fa {
     padding: initial;
    font-size: 21px;
    height: 35px;
    width: 300px;
    text-align: center;
    text-decoration: none;
    margin: 5px 2px;
    border-style: inset;
    
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}
        
    </style>
     <script type="text/javascript">
        
        function pageLoad() {

            $(document).ready(function () {

               

                 //Fire Button Click event to check login
                $("#btnLogin").click(function () {

                    //alert("Button Click Working");

                    // read value from TextBoxes.
                    var firstName = $("#txtUserName").val();
                    var lastName = $("#txtPassword").val();

                    //alert(firstName + " " + lastName);

                    // call Asp.net Web api
                    PageMethods.CheckLoginApi(firstName, lastName,
                            function (result) {

                                    // Display Message Panel with Message

                              
                                var resultSet = result.search("not");

                             
                                if (resultSet == -1)
                                {
                                    window.location.href = "WelcomePage.aspx";
                                }
                                else
                                {
                                    $("#lblMessage").text(result);
                                    $("#divMessagePanel")
                                        .show()
                                        .fadeOut(5000);
                                }
                                        

                                   

                        });

                });

            });

        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="scriptManger" runat="server" EnablePageMethods="true">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                     <div class="w3-panel w3-gray w3-card-4" >
                            
                             <table>
                            <tr>
                                <td>
                                    <input type="text" id="txtUserName" placeholder="User Name" />
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <input type="password" id="txtPassword"  placeholder="Password" />
                                </td>
                            </tr>
                             <tr>
                                <td colspan="2">
                                       <input type="button" id="btnLogin" value="Login" class="fa fa-facebook"/>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                       <div id="divMessagePanel" >
                                            <span id="lblMessage"></span>
                                        </div>
                                </td>
                            </tr>
                        </table>

                    </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
