﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ServerFntnToClientSideLOGIN
{
    public class DalRepository
    {
        public int? status = null;
        public string message = null;

        private LoginDCDataContext _db = null;

        public DalRepository()
        {
            _db = new LoginDCDataContext();
        }

        public int CheckLoginAsync(LoginEntity LoginEntity)
        {
            

            _db
                ?.uspLoginCheck
                ("CheckLogin"
                ,LoginEntity?.UserName
                ,LoginEntity?.Password
                ,ref status
                ,ref message

                    );
            return (int)status;

        }
    }
}